Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sk1libs
Upstream-Contact: Igor E. Novikov <igor.e.novikov@gmail.com>
Source:  http://sk1project.org/

Files: ./src/imaging/*
       ./src/pycms/Imaging.h
Copyright: 1997-2009 by Secret Labs AB
           1995-2009 by Fredrik Lundh
	   1998, Toby J Sargeant
	   2004, Bob Ippolito
	   2004, Health Research Inc. (HRI) RENSSELAER, NY 12144
	   2004, William Baxter
	   2008, Karsten Hiddemann
	   1998-2001, Marti Maria
	   2002-2003, Kevin Cazabon
License: BSD-3-clause like
 Python Imaging Library
 .
 By obtaining, using, and/or copying this software and/or its
 associated documentation, you agree that you have read, understood,
 and will comply with the following terms and conditions:
 .
 Permission to use, copy, modify, and distribute this software and its
 associated documentation for any purpose and without fee is hereby
 granted, provided that the above copyright notice appears in all
 copies, and that both that copyright notice and this permission notice
 appear in supporting documentation, and that the name of Secret Labs
 AB or the author not be used in advertising or publicity pertaining to
 distribution of the software without specific, written prior
 permission.
 .
 SECRET LABS AB AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO
 THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS.  IN NO EVENT SHALL SECRET LABS AB OR THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files: ./src/libpdf/*
Copyright: 2000-2008 ReportLab Inc. <info@reportlab.com>
 ReportLab Europe Ltd. 2000-2010
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
  * Neither the name of the company nor the names of its contributors
    may be used to endorse or promote products derived from this
    software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE OFFICERS
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: ./src/filters/*
Copyright: 1997-2003, Bernhard Herzog
           2003-2010, Igor Novikov
	   2009, Barabash Maxim
	   2001-2002, David Boddie
License: LGPL-2+
 On Debian systems, the complete text of the GNU Library General
 Public License can be found in `/usr/share/common-licenses/LGPL-2'.

Files: ./src/filters/formats/drawfile.py
 ./src/filters/formats/spritefile.py
Copyright: David Boddie 2001-2002
License: public-domain
 This module may be freely distributed and modified.

Files: ./src/pycms/__init__.py
 ./src/pycms/_pycms.c
 ./src/utils/system.py
Copyright: 2009-2010, Igor E.Novikov
License: LGPL-2+

Files: ./setup.py
 ./setup_win32.py
Copyright: 2009-2010, Igor E. Novikov
License: LGPL-2.1+

Files: ./src/ft2engine/libft2/ft2module.c
Copyright: 2002, Abel Deuring <a.deuring@satzbau-gmbh.de>
  2007, Igor E.Novikov <igor.e.novikov@gmail.com>
License: GPL-2+

License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2+
 On Debian systems, the complete text of the GNU Library General
 Public License can be found in `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2.1+
 On Debian systems, the complete text of the GNU Library General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.
